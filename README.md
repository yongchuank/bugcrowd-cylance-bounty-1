Vulnerabilities in Cylance products can only be reported through BugCrowd and Hackerone.  
Under BugCrowd terms and conditions, Cylance explicitly does not allow disclosure of this issue.  

Timeline:  
2016-12-27	Reported vulnerability to Cylance via BugCrowd, as directed in https://www.cylance.com/security  
2016-12-27	Cylance acknowledged submission  
2017-01-11	Cylance accepted vulnerability as valid, and awarded $100 and 10 kudos points  
2017-01-11	Queried if a CVE will be assigned  
2017-01-12	Cylance replied that a CVE will not be assigned  
2017-01-13	Queried for an estimated patch release date to coordinate public disclosure  
2017-01-13	Cylance replied that the bug bounty program requires vendor’s explicit permission to disclose. In this case, Cylance is not giving permission to disclose  
2017-01-26	Queried for an estimated patch release date, and suggested how it may be fixed  
2017-01-26	Cylance replied the release date is estimated to be in March  
2017-03-14	Claimed and donated bounty to Metta Welfare Association  
2017-05-04	Cylance replied that this is fixed in version 1430  